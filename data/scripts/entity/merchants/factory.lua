package.path = package.path .. ";data/scripts/lib/?.lua"

local ConsumerGoods = include ("consumergoods")

local MineConsumerInitialize = Factory.initialize
function Factory.initialize(...)
	MineConsumerInitialize(...)
	
	local station = Entity()
	if (station:getValue("factory_type") == "mine" and not station:hasScript("data/scripts/entity/merchants/consumer.lua")) then
		station:addScriptOnce("data/scripts/entity/merchants/consumer.lua", "Mine"%_t, unpack(ConsumerGoods.Mine()))
	end
end